# Engineering Assessment - Pop Movies

This project is the engineering assessment of Filipe Falcão for Red Thread Innovations.

To start the app in development mode, run `yarn start` in the project directory. To build the app in production mode, run `yarn build`. Run `yarn test` to launch the test runner and run the Jest tests.

## TMDB API Key

To run the app, create a `.env` file with the same variables from `.env.sample`. Replace "YOUR_API_KEY" with your TMDB API key and you will be set to go.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
