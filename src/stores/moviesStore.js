import { action, makeObservable, observable } from "mobx";
import moviesService from "../services/moviesService";

class MoviesStore {
  state = "pending"; // "pending", "done", or "error"
  errorObject = Error;
  movies = [];
  selectedMovie = null;
  trailers = [];

  constructor() {
    makeObservable(this, {
      state: observable,
      errorObject: observable,
      movies: observable,
      selectedMovie: observable,
      trailers: observable,
      fetchMovies: action,
      fetchMovieDetails: action,
      setState: action,
      setErrorState: action,
      setMovies: action,
      setSelectedMovie: action,
      setTrailers: action,
    });
  }

  async fetchMovies() {
    this.setState("pending");
    this.setMovies([]);

    try {
      const response = await moviesService.getMovies();
      this.setMovies(response.results);
      this.setState("done");
    } catch (e) {
      this.setErrorState(e);
    }
  }

  async fetchMovieDetails(movieId) {
    this.setState("pending");
    this.setSelectedMovie(null);
    this.setTrailers([]);

    try {
      const selectedMovie = await moviesService.getMovieDetails(movieId);
      const movieVideos = await moviesService.getMovieVideos(movieId);
      this.setSelectedMovie(selectedMovie);
      this.setTrailers(movieVideos);
      this.setState("done");
    } catch (e) {
      this.setErrorState(e);
    }
  }

  setState(state) {
    this.state = state;
  }

  setErrorState(error) {
    this.state = "error";
    this.errorObject = error;
  }

  setMovies(movies) {
    this.movies = movies;
  }

  setSelectedMovie(selectedMovie) {
    this.selectedMovie = selectedMovie;
  }

  setTrailers(movieVideos) {
    this.trailers = movieVideos.results?.filter(
      (video) =>
        video.type === "Trailer" && video.site === "YouTube" && video.official
    );
    this.trailers?.reverse();
  }
}

const moviesStore = new MoviesStore();
export default moviesStore;
