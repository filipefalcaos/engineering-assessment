import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import AppHeader from "./components/AppHeader";
import Home from "./pages/home";
import MovieDetails from "./pages/movie-details";
import PageNotFound from "./pages/404";

const App = () => {
  return (
    <>
      <Router>
        <AppHeader />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/movie-details/:id" element={<MovieDetails />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </Router>
    </>
  );
};

export default App;
