import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { AppBar, Box, IconButton, Toolbar, Typography } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";

const AppHeader = () => {
  const location = useLocation();
  const regex = /\/movie-details\/\d+/;
  const isDetailsPage = regex.test(location.pathname);

  const navigate = useNavigate();
  const goBack = () => {
    navigate(-1);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="secondary">
        <Toolbar>
          {isDetailsPage && (
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={goBack}
            >
              <ArrowBackIcon fontSize="small" />
            </IconButton>
          )}
          <Typography variant="body1" sx={{ flexGrow: 1 }}>
            {isDetailsPage ? "Movie details" : "Pop Movies"}
          </Typography>
          <IconButton
            size="large"
            edge="end"
            color="inherit"
            aria-label="options"
          >
            <MoreVertIcon fontSize="small" />
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default AppHeader;
