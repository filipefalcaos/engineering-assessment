import React from "react";
import { styled } from "@mui/material/styles";
import { Typography } from "@mui/material";

const ErrorDisplay = ({ errorMessage }) => {
  const ErrorWrapper = styled("div")(({ theme }) => ({
    position: "absolute",
    left: "50%",
    top: "50%",
    width: "90%",
    WebkitTransform: "translate(-50%, -50%)",
    transform: "translate(-50%, -50%)",
    textAlign: "center",
    color: theme.palette.text.main,
  }));

  return (
    <ErrorWrapper>
      <Typography variant="h6">Oops! Something went wrong...</Typography>
      <Typography>{`Error: ${errorMessage}`}</Typography>
    </ErrorWrapper>
  );
};

export default ErrorDisplay;
