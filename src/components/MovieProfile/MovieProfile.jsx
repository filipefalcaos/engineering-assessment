import React from "react";
import { observer } from "mobx-react";
import { Container, Divider, Toolbar, Typography } from "@mui/material";

import moviesStore from "../../stores/moviesStore";
import ProfileMainData from "./ProfileMainData";
import TrailersData from "./TrailersData";

const MovieProfile = observer(() => {
  return (
    moviesStore.selectedMovie && (
      <>
        <Toolbar sx={{ bgcolor: "primary.main" }}>
          <Typography variant="body1" sx={{ flexGrow: 1, color: "#FFFFFF" }}>
            {moviesStore.selectedMovie.title}
          </Typography>
        </Toolbar>
        <Container sx={{ margin: "16px 0px" }}>
          <ProfileMainData />

          <Typography variant="body2" style={{ margin: "20px 0px" }}>
            {moviesStore.selectedMovie.overview}
          </Typography>

          <Typography variant="body2">TRAILERS</Typography>
          <Divider />
          <TrailersData />
        </Container>
      </>
    )
  );
});

export default MovieProfile;
