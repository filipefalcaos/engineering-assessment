import React from "react";
import { observer } from "mobx-react";
import { Button, Grid, Typography } from "@mui/material";
import moviesStore from "../../../stores/moviesStore";

const ProfileHeader = observer(() => {
  const tmdbBaseUrl = "https://image.tmdb.org/t/p/w185";
  return (
    <Grid container direction="row" spacing={2}>
      <Grid item sx={{ width: "40%" }}>
        <img
          src={`${tmdbBaseUrl}${moviesStore.selectedMovie.poster_path}`}
          alt={moviesStore.selectedMovie.title}
          style={{ display: "block", width: "100%" }}
        />
      </Grid>
      <Grid item xs>
        <Grid container direction="column" sx={{ height: "100%" }}>
          <Grid item>
            <Typography variant="body1">
              {moviesStore.selectedMovie.release_date.split("-")[0]}
            </Typography>
          </Grid>
          <Grid item sx={{ flexGrow: 1 }}>
            <Typography variant="caption" sx={{ fontStyle: "italic" }}>
              {moviesStore.selectedMovie.runtime} mins
            </Typography>
          </Grid>
          <Grid item sx={{ flexGrow: 1 }}>
            <Typography variant="caption" sx={{ fontWeight: 600 }}>
              {moviesStore.selectedMovie.vote_average}/10
            </Typography>
          </Grid>
          <Grid item>
            <Button variant="contained" fullWidth>
              Add to Favorite
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
});

export default ProfileHeader;
