import React from "react";
import { observer } from "mobx-react";
import { styled } from "@mui/material/styles";
import { Grid, Typography } from "@mui/material";
import PlayCircleOutlineIcon from "@mui/icons-material/PlayCircleOutline";

import moviesStore from "../../../stores/moviesStore";

const TrailersData = observer(() => {
  const youtubeBaseUrl = "https://www.youtube.com/watch?v=";

  const openInNewTab = (url) => {
    const newWindow = window.open(url, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  };

  const TrailerWrapper = styled(Grid)(() => ({
    padding: 12,
    marginTop: 8,
    backgroundColor: "#f5f5f5",
  }));

  return (
    <div style={{ marginTop: 12 }}>
      {moviesStore.trailers.length ? (
        moviesStore.trailers.map((trailer, i) => (
          <TrailerWrapper
            container
            direction="row"
            alignItems="center"
            key={trailer.id}
            onClick={() => openInNewTab(`${youtubeBaseUrl}${trailer.key}`)}
          >
            <PlayCircleOutlineIcon sx={{ color: "text.main" }} />
            <Typography variant="body2" style={{ marginLeft: 12 }}>
              Play trailer {i + 1}
            </Typography>
          </TrailerWrapper>
        ))
      ) : (
        <Typography variant="body2">No trailers available.</Typography>
      )}
    </div>
  );
});

export default TrailersData;
