import React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import { styled } from "@mui/material/styles";

const PageLoading = () => {
  const LoadingWrapper = styled("div")(() => ({
    position: "absolute",
    left: "50%",
    top: "50%",
    WebkitTransform: "translate(-50%, -50%)",
    transform: "translate(-50%, -50%)",
  }));

  return (
    <LoadingWrapper>
      <CircularProgress sx={{ color: "secondary.main" }} />
    </LoadingWrapper>
  );
};

export default PageLoading;
