import React from "react";
import { observer } from "mobx-react";
import { useNavigate } from "react-router-dom";
import {
  Container,
  ImageList,
  ImageListItem,
  useMediaQuery,
  Typography,
} from "@mui/material";

import moviesStore from "../../stores/moviesStore";

const PostersGrid = observer(() => {
  const navigate = useNavigate();
  const isTablet = useMediaQuery("(min-width:768px)");
  const imgsPerRow = isTablet ? 4 : 2;

  const goToDetailsPage = (movieId) => {
    navigate(`/movie-details/${movieId}`);
  };

  return moviesStore.movies.length ? (
    <ImageList cols={imgsPerRow} gap={0} sx={{ margin: 0 }}>
      {moviesStore.movies.map((movie) => (
        <ImageListItem key={movie.id} onClick={() => goToDetailsPage(movie.id)}>
          <img
            src={`https://image.tmdb.org/t/p/w342${movie.poster_path}`}
            alt={movie.title}
          />
        </ImageListItem>
      ))}
    </ImageList>
  ) : (
    <Container>
      <Typography sx={{ marginTop: "16px" }}>No movies available.</Typography>
    </Container>
  );
});

export default PostersGrid;
