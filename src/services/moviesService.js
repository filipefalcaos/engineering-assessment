class MoviesService {
  constructor() {
    this.baseURL = process.env.REACT_APP_TMDB_API_BASE_URL;
    this.apiKey = process.env.REACT_APP_TMDB_API_KEY;
  }

  async getMovies() {
    try {
      const response = await fetch(
        `${this.baseURL}popular?api_key=${this.apiKey}`
      );
      return response.json();
    } catch (e) {
      throw new Error("Failed to retrieve popular movies from TMDB.");
    }
  }

  async getMovieDetails(movieId) {
    try {
      const response = await fetch(
        `${this.baseURL}${movieId}?api_key=${this.apiKey}`
      );
      return response.json();
    } catch (e) {
      throw new Error("Failed to retrieve movie details from TMDB.");
    }
  }

  async getMovieVideos(movieId) {
    try {
      const response = await fetch(
        `${this.baseURL}${movieId}/videos?api_key=${this.apiKey}`
      );
      return response.json();
    } catch (e) {
      throw new Error(`Failure to retrieve movie videos from TMDB.`);
    }
  }
}

const moviesService = new MoviesService();
export default moviesService;
