import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Home from "./Home";

// Mocks react router's "useNavigate"
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => jest.fn(),
}));

// Handles the setup of a target element
let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

// Handles the cleanup of the target element
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const fakeMovies = {
  results: [
    {
      id: 12345,
      poster_path: "/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg",
      title: "Fake Movie",
    },
    {
      id: 678910,
      poster_path: "/d5NXSklXo0qyIYkgV94XAgMIckC.jpg",
      title: "Fake Movie 2",
    },
  ],
};

const noMovies = {
  results: [],
};

describe("Home component", () => {
  it("should render a list of movie posters", async () => {
    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(fakeMovies),
      })
    );

    await act(async () => render(<Home />, container));

    const imgs = container.querySelectorAll("img");
    expect(imgs).toHaveLength(2);
    expect(imgs[0].alt).toBe(fakeMovies.results[0].title);
    expect(imgs[1].alt).toBe(fakeMovies.results[1].title);
  });

  it("should inform if no movies are available", async () => {
    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(noMovies),
      })
    );

    await act(async () => render(<Home />, container));

    expect(container.querySelectorAll("img")).toHaveLength(0);
    expect(container.textContent).toContain("No movies available.");
  });
});
