import React, { useEffect } from "react";
import { observer } from "mobx-react";

import moviesStore from "../../stores/moviesStore";
import ErrorDisplay from "../../components/ErrorDisplay";
import PageLoading from "../../components/PageLoading";
import PostersGrid from "../../components/PostersGrid";

const Home = observer(() => {
  useEffect(() => {
    const fetchData = async () => {
      await moviesStore.fetchMovies();
      if (moviesStore.state === "error") {
        console.error(moviesStore.errorMessage);
      }
    };

    fetchData();
  }, []);

  return moviesStore.state === "error" ? (
    <ErrorDisplay errorMessage={moviesStore.errorObject.message} />
  ) : moviesStore.state === "pending" ? (
    <PageLoading />
  ) : (
    <PostersGrid />
  );
});

export default Home;
