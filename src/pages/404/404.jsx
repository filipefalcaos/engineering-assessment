import React from "react";
import { Button, Typography } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import { styled } from "@mui/material/styles";

const MessageWrapper = styled("div")(({ theme }) => ({
  position: "absolute",
  left: "50%",
  top: "50%",
  WebkitTransform: "translate(-50%, -50%)",
  transform: "translate(-50%, -50%)",
  textAlign: "center",
  color: theme.palette.text.main,
}));

const PageNotFound = () => {
  return (
    <MessageWrapper>
      <Typography variant="h2">Oops!</Typography>
      <Typography sx={{ margin: "8px 0px" }}>404 - Page not found</Typography>
      <Button
        component={RouterLink}
        to="/"
        variant="contained"
        sx={{ marginTop: "4px" }}
      >
        Go to Homepage
      </Button>
    </MessageWrapper>
  );
};

export default PageNotFound;
