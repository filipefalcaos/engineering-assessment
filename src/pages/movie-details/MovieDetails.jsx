import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { useParams } from "react-router-dom";

import moviesStore from "../../stores/moviesStore";
import ErrorDisplay from "../../components/ErrorDisplay";
import MovieProfile from "../../components/MovieProfile";
import PageLoading from "../../components/PageLoading";

const MovieDetails = observer(() => {
  let { id } = useParams();

  useEffect(() => {
    const fetchData = async (id) => {
      await moviesStore.fetchMovieDetails(id);
      if (moviesStore.state === "error") {
        console.error(moviesStore.errorMessage);
      }
    };

    fetchData(id);
  }, [id]);

  return moviesStore.state === "error" ? (
    <ErrorDisplay errorMessage={moviesStore.errorObject.message} />
  ) : moviesStore.state === "pending" ? (
    <PageLoading />
  ) : (
    <MovieProfile />
  );
});

export default MovieDetails;
