import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import MovieDetails from "./MovieDetails";

// Handles the setup of a target element
let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

// Handles the cleanup of the target element
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const fakeMovieDetails = {
  overview: "A fake overview of a fake movie",
  poster_path: "/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg",
  release_date: "2021-09-30",
  runtime: 90,
  title: "Fake Movie",
  vote_average: 7.2,

  // Fake movie videos
  // Only two match the trailer criteria in "moviesStore"
  results: [
    {
      id: 12345,
      key: "rjkmN1dniUHVYAtwuV3Tji7FsDO",
      type: "Trailer",
      site: "YouTube",
      official: true,
    },
    {
      id: 67891,
      key: "d5NXSklXo0qyIYkgV94XAgMIckC",
      type: "Clip",
      site: "YouTube",
      official: true,
    },
    {
      id: 34567,
      key: "rjkmN1dndhsgfujst3Tji7FsDO",
      type: "Trailer",
      site: "YouTube",
      official: true,
    },
  ],
};

const fakeMovieDetailsNoTrailers = {
  overview: "A fake overview of a fake movie",
  poster_path: "/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg",
  release_date: "2021-09-30",
  runtime: 90,
  title: "Fake Movie",
  vote_average: 7.2,
  results: [],
};

describe("MovieDetails component", () => {
  it("should render movie details", async () => {
    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(fakeMovieDetails),
      })
    );

    await act(async () => render(<MovieDetails />, container));

    expect(container.textContent).toContain(fakeMovieDetails.title);
    expect(container.textContent).toContain(fakeMovieDetails.overview);
    expect(container.textContent).toContain(`${fakeMovieDetails.runtime} mins`);
    expect(container.textContent).toContain(
      fakeMovieDetails.release_date.split("-")[0]
    );
    expect(container.textContent).toContain(
      `${fakeMovieDetails.vote_average}/10`
    );
  });

  it("should render the movie poster", async () => {
    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(fakeMovieDetails),
      })
    );

    await act(async () => render(<MovieDetails />, container));

    const img = container.querySelector("img");
    expect(img.alt).toBe(fakeMovieDetails.title);
  });

  it("should render a list of movie trailers", async () => {
    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(fakeMovieDetails),
      })
    );

    await act(async () => render(<MovieDetails />, container));

    expect(container.textContent).toContain("Play trailer 1");
    expect(container.textContent).toContain("Play trailer 2");
  });

  it("should inform if no movie trailers are available", async () => {
    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(fakeMovieDetailsNoTrailers),
      })
    );

    await act(async () => render(<MovieDetails />, container));

    expect(container.textContent).toContain("No trailers available.");
  });
});
