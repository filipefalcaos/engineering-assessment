import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#605752",
    },
    secondary: {
      main: "#191919",
    },
    text: {
      main: "#5c5c5c",
    },
  },
  typography: {
    fontFamily: `"Roboto", "Helvetica Neue", sans-serif`,
    body2: {
      color: "#5c5c5c",
    },
    button: {
      color: "white",
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          height: 42,
          borderRadius: 0,
          textTransform: "none",
        },
      },
      defaultProps: {
        disableElevation: true,
      },
    },
  },
});

export default theme;
